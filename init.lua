local formspec = "size[12,4]\n"..
   "pwdfield[1,1;10,1;password;New password:]\n"..
   "button_exit[1,2;2,1;cancel;cancel]\n" ..
   "button_exit[4,2;2,1;ok;ok]"

local formname = "webgazmanage:change_password"

local change_password = function(player_name)
   minetest.show_formspec(player_name, formname, formspec)
end

local handle = function (player, given_formname, fields)
   if given_formname == formname and fields.password and not fields.cancel then
      local name = player:get_player_name()
      local hash = minetest.get_password_hash(name, fields.password)
      minetest.set_player_password(name, hash)
      minetest.chat_send_player(name, "Changed password.")
   end
end

minetest.register_on_player_receive_fields(handle)
minetest.register_chatcommand("change_password",
                              { description = "Change your password",
                                func = change_password })
